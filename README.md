# Project Add 2 numbers - Hướng dẫn sử dụng
IDE: IntelliJ IDEA Community Edition
## Biên dịch và chạy ứng dụng
## Biên dịch và chạy ứng dụng

1. **Biên dịch**: Để biên dịch ứng dụng, bạn cần có JDK (Java Development Kit) được cài đặt trên máy tính của bạn.  
    Di chuyển vào file main.java 
    Bấm nút mũi tên màu xanh ![img.png](img.png) -> xuất cửa sổ  ![img_1.png](img_1.png)
    chọn Run 'Main.main()'

    
## Chạy các Test Case
Để chạy các test case, chúng ta sử dụng JUnit, một framework phổ biến cho việc kiểm thử trong Java.
Đảm bảo rằng bạn đã cài đặt JUnit trước khi chạy các bước dưới đây.
Để chạy unit test 
    1. Di chuyển vào file MyBigNumberTest
    2. Chọn Nút mũi tên bên trái ![img_2.png](img_2.png) -> xuất cửa sổ ![img_3.png](img_3.png)
    3. Chọn Run 'testSum()'


