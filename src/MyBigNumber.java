import java.util.logging.Logger;

public class MyBigNumber {
    private static final Logger LOGGER = Logger.getLogger(MyBigNumber.class.getName());
    public String sum(String stn1, String stn2) {
        StringBuilder result = new StringBuilder();
        StringBuilder lichsu = new StringBuilder();
        int nho = 0;
        int i = stn1.length() - 1;
        int j = stn2.length() - 1;
        // Duyệt từ phải sang trái các kí tự của chuỗi
        while (i >= 0 || j >= 0) {
            int digit1 = (i >= 0) ? Character.getNumericValue(stn1.charAt(i)) : 0; // Lấy kí số tương ứng của stn1
            int digit2 = (j >= 0) ? Character.getNumericValue(stn2.charAt(j)) : 0; // Lấy kí số tương ứng của stn2
            int sum = digit1 + digit2 + nho;
            nho = sum / 10;
            result.insert(0, sum % 10);
            lichsu.insert(0, "(" + digit1 + " + " + digit2 + " + " + nho + ") ");
            i--;
            j--;
        }
            if (nho > 0) {
                result.insert(0, nho);
            }
            LOGGER.info("Lịch sử phép toán: " + lichsu.toString());
            return result.toString();
        }
}
