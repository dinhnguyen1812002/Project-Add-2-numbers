import org.junit.Test;
import static org.junit.Assert.*;
public class  MyBigNumberTest {

    @Test
    public void testSum() {
        MyBigNumber bigNumber = new MyBigNumber();

        // Test cộng hai số có cùng độ dài
        assertEquals("666", bigNumber.sum("333", "333"));

        // Test cộng hai số có độ dài khác nhau
        assertEquals("2131", bigNumber.sum("1234", "897"));

        // Test cộng hai số có độ dài lớn
        assertEquals("123456789012345678901234567890",
                bigNumber.sum("123456789012345678901234567800", "90"));

        // Test cộng hai số với số 0
        assertEquals("12345678901234567890", bigNumber.sum("12345678901234567890", "0"));

        // Test cộng hai số lớn
        assertEquals("100000000000000000000",
                bigNumber.sum("99999999999999999999", "1"));
    }
}
